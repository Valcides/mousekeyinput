/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Capturas;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jnativehook.mouse.NativeMouseWheelEvent;
import org.jnativehook.mouse.NativeMouseWheelListener;

/**
 *
 * @author Valcides C. Moraes Classe que cuida dos eventos do Scroll do mouse
 */
public class CaptureMouseWheel implements NativeMouseWheelListener {

    private boolean ativo;
    private int limiteSuperior;
    private int limiteInferior;
    private int letraAtual;

//Variável que carregará a letra corrente e que será impressa no editor
    // 65 é a letra A
    //90 é a letra Z
    //Este é o intervalo com o qual estamos trabalahndo aqui.
    public int getLimiteSuperior() {
        return limiteSuperior;
    }

    public void setLimiteSuperior(int limiteSuperior) {
        this.limiteSuperior = limiteSuperior;
    }

    public int getLimiteInferior() {
        return limiteInferior;
    }

    public void setLimiteInferior(int limiteInferior) {
        this.limiteInferior = limiteInferior;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public int getLetraAtual() {
        return letraAtual;
    }

    public void setLetraAtual(int letraAtual) {
        this.letraAtual = letraAtual;
    }

    public CaptureMouseWheel() {
        ativo = true;
    }

    void verificaIntervalo() {
        //Método que força o nosso intervalo de letras a permanecer entre 65 e 90.
        letraAtual = (letraAtual > limiteSuperior) ? limiteInferior :(letraAtual < limiteInferior) ? limiteSuperior : letraAtual; //Caso seja maior que 90 volta para 65//Caso seja menor que 65 volta para 90

    }

    public void nativeMouseWheelMoved(NativeMouseWheelEvent e) {
        if (ativo) {
            //Método executado a cada alteração no Scroll
            Robot robot = null; //Objeto que cuida da inputação das teclas.
            try {
                robot = new Robot();
            } catch (AWTException ex) {
                Logger.getLogger(CaptureMouseWheel.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("Mosue Wheel Moved: " + e.getWheelRotation());
            if (e.getWheelRotation() == -1) {
                letraAtual--;
                //Diminui a letra se for movido para cima
                System.out.println("Scroll do moude movido para cima!");
            } else {
                letraAtual++;
                //Aumenta a letra se for movido para baixo
                System.out.println("Scroll do moude movido para baixo!");
            }
            verificaIntervalo();
            robot.keyPress(KeyEvent.VK_BACK_SPACE);//Apaga a letra anterior inputando o BackSpacae
            System.out.println("Código da letra atual: " + letraAtual);
            robot.keyPress(letraAtual);
//Inputa nossa letra atual
        }
    }
  }
