/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Capturas;

import java.awt.AWTException;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

/**
 *
 * @author Valcides C. Moraes
 */
public class CapturaGeral {

    public static void main(String[] A) throws AWTException {
        
        try {
            GlobalScreen.registerNativeHook();//Inicializando GlobalScreen 
        } catch (NativeHookException ex) {
            System.err.println("Ocorreu um problema inesperado com o registro");
            System.err.println(ex.getMessage());

            System.exit(1);
        }
        
        CaptureMouseWheel cmw = new CaptureMouseWheel(); //Objeto da classe que manipulará o Scroll do mouse
        CaptureMouse captureMouse = new CaptureMouse(cmw,new Forms.Principal()); //Objeto da classe que manipulará todos os tipos de cliques e outros gestos do mouse

        //Adição dos meus objetos à GlobalScreen
        GlobalScreen.getInstance().addNativeMouseListener(captureMouse);
        GlobalScreen.getInstance().addNativeMouseMotionListener(captureMouse);
        GlobalScreen.getInstance().addNativeMouseWheelListener(cmw);

    }

}
