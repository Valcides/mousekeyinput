/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Capturas;

import Forms.PopupTextField;
import Forms.Principal;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseInputListener;

/**
 *
 * @author Valcides C. Moraes Classe de manipulação dos gestos e cliques de
 * mouse
 */
public class CaptureMouse implements NativeMouseInputListener {

    Robot r;
    Principal p;
    PopupTextField pt;
    boolean estaArrastando = false;//Previne que o mouse fique inputando coisas quando o usuario estiver selecionando um texto
    CaptureMouseWheel cmw; //Objeto que usamos para controlar o Scroll, a parte desete objeto podemos ter um controle da letra atual em todo o projeto

    public CaptureMouse(CaptureMouseWheel cmw, Principal p) {
        cmw.setAtivo(true);
        this.p = p;
        this.cmw = cmw;
        try {
            r = new Robot();
        } catch (AWTException ex) {
            Logger.getLogger(CaptureMouse.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public int getLimiteSuperior() {
        return cmw.getLimiteSuperior();
    }

    public void setLimiteSuperior(int limiteSuperior) {
        cmw.setLimiteSuperior(limiteSuperior);
    }

    public int getLimiteInferior() {
        return cmw.getLimiteInferior();
    }

    public void setLetraAtual(int letraAtual) {
        cmw.setLetraAtual(letraAtual);
    }

    public int getLetraAtual() {
        return cmw.getLetraAtual();
    }

    public void setLimiteInferior(int limiteInferior) {
        cmw.setLimiteInferior(limiteInferior);
    }

    public void setAtivo(boolean ativo) {
        this.cmw.setAtivo(ativo);
    }

    public boolean isAtivo() {
        return this.cmw.isAtivo();
    }

    public void nativeMouseClicked(NativeMouseEvent e) {
        System.out.println("Mosue Clicked: " + e.getClickCount());
    }

    public void nativeMousePressed(NativeMouseEvent e) {
    }

    public void nativeMouseReleased(NativeMouseEvent e) {
        //Método disparado a cada clique
        if (cmw.isAtivo()) {
            if (e.getButton() == 2) {
                r.keyPress(KeyEvent.VK_ESCAPE);
                if (e.getClickCount() == 1) {
                    r.keyPress(KeyEvent.VK_ENTER);
                    setLetraAtual(getLimiteInferior());
                    r.keyPress(getLetraAtual());
                } else {
                    r.keyPress(KeyEvent.VK_BACK_SPACE);
                     r.keyPress(KeyEvent.VK_BACK_SPACE);
                     r.keyPress(KeyEvent.VK_BACK_SPACE);
                }
            } else if (e.getButton() == 3) {
                if (e.getClickCount() == 1) {
                    r.keyPress(KeyEvent.VK_SPACE);
                    setLetraAtual(getLimiteInferior());
                    r.keyPress(getLetraAtual());
                } else {
                    System.out.print("sdsdsdsds");
                    pt = new PopupTextField(this.p, this);
                    boolean min = p.isShowing();
                    pt.show(p.getComponent(0), e.getX(), e.getY());

                }
            } else if (estaArrastando) {
                estaArrastando = false;
            } else {

                try {
                    setLetraAtual(getLimiteInferior());//Volta a letra atual para A
                    r.keyPress(getLetraAtual());//Inputa a letra

                } catch (Exception eca) {

                }
            }
        }
    }

    public void nativeMouseMoved(NativeMouseEvent e) {
        System.out.println("Mosue Moved: " + e.getX() + ", " + e.getY());
    }

    public void nativeMouseDragged(NativeMouseEvent e) {
        if (cmw.isAtivo()) {
            estaArrastando = true;
            System.out.println("Mosue Dragged: " + e.getX() + ", " + e.getY());
        }
    }

}
