package ImagemPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


 
 import java.awt.*; 
 import java.awt.geom.Rectangle2D; 
 import java.awt.image.BufferedImage; 
 import java.io.File; 
 import java.io.IOException; 
 
 
 import javax.imageio.ImageIO; 
 import javax.swing.JPanel; 
 

 
 
 /** 
15  * A panel that contains a background image. The background image is 
16  * automatically sized to fit in the panel. 
17  *  
18  * @author Vinicius Godoy 
19  */ 
 public class JImagePanel extends JPanel { 
	private LoopImage images; 
	private FillType fillType = FillType.RESIZE; 
     private volatile boolean loop = true; 
 
 
 	/** 
26 	 * Creates a new panel with the given background image. 
27 	 *  
28 	 * @param img 
29 	 *            The background image. 
30 	 */ 
 	public JImagePanel(BufferedImage img) { 
 		images = new LoopImage(0, img); 
 	} 
 
 
 	/** 
36 	 * Creates a new panel with the given background images looping at each tick 
37 	 * interval. 
38 	 *  
39 	 * @param tick 
40 	 *            the time between swap the image 
41 	 * @param imgs 
42 	 *            The background images. 
43 	 */ 
 	public JImagePanel(long tick, BufferedImage... imgs) { 
 		images = new LoopImage(tick, imgs); 
 		new Looper().start(); 
 	} 
 
 
 	/** 
50 	 * Creates a new panel with the given background image. 
51 	 *  
52 	 * @param imgSrc 
53 	 *            The background image. 
54 	 * @throws IOException 
55 	 *             , if the image file is not found. 
56 	 */ 
 	public JImagePanel(File imgSrc) throws IOException { 
 		this(ImageIO.read(imgSrc)); 
 	} 
 
 
 	/** 
62 	 * Default constructor, should be used only for sub-classes 
63 	 */ 
 	public JImagePanel() { 
 
 
 	} 
 
 
 	/** 
9 	 * Creates a new panel with the given background image. 
70 	 *  
71 	 * @param fileName 
72 	 *            The background image. 
73 	 * @throws IOException 
74 	 *             , if the image file is not found. 
75 	 */ 
 	public JImagePanel(String fileName) throws IOException { 
 		this(new File(fileName)); 
 	} 
 
 
 	/** 
81 	 * Changes the image panel image. 
82 	 *  
83 	 * @param img 
84 	 *            The new image to set. 
85 	 */ 
 	public final void setImage(BufferedImage img) { 
         this.images = img == null ? null : new LoopImage(0, img); 
 		invalidate(); 
 	} 
 
 
 	/** 
92 	 * Changes the image panel image. 
93 	 *  
94 	 * @param img 
95 	 *            The new image to set. 
96 	 * @throws IOException 
97 	 *             If the file does not exist or is invalid. 
98 	 */ 
 	public void setImage(File img) throws IOException { 
         if (img == null) 
             images = null; 
         else 
 		    setImage(ImageIO.read(img)); 
 	} 
 
 
 	/** 
107 	 * Changes the image panel image. 
108 	 *  
109 	 * @param fileName 
110 	 *            The new image to set. 
111 	 * @throws IOException 
112 	 *             If the file does not exist or is invalid. 
113 	 */ 
 	public void setImage(String fileName) throws IOException { 
         if (fileName == null) 
             images = null; 
         else 
 		    setImage(new File(fileName)); 
 	} 
 
 
 	/** 
122 	 * Returns the image associated with this image panel. 
123 	 *  
124 	 * @return The associated image. 
125 	 */ 
 	public BufferedImage getImage() { 
 		return images == null ? null : images.getCurrent(); 
 	} 
 
 
 	@Override 
 	protected void paintComponent(Graphics g) { 
 		super.paintComponent(g); 
         if (images == null) 
             return; 
 
 
 		Graphics2D g2d = (Graphics2D) g.create(); 
 		fillType.drawImage(this, g2d, images.getCurrent()); 
 		g2d.dispose(); 
 	} 
 
 
 	/** 
142 	 * Returns the way this image fills itself. 
143 	 *  
144 	 * @return The fill type. 
145 	 */ 
 	public FillType getFillType() { 
 		return fillType; 
 	} 
 
 
 	/** 
151 	 * Changes the fill type. 
152 	 *  
153 	 * @param fillType 
154 	 *            The new fill type 
155 	 * @throws IllegalArgumentException 
156 	 *             If the fill type is null. 
157 	 */ 
 	public void setFillType(FillType fillType) { 
 		if (fillType == null) 
 			throw new IllegalArgumentException("Invalid fill type!"); 
 
 		this.fillType = fillType; 
 		invalidate(); 
 	} 
 
 
 	public static enum FillType { 
 		/** 
168 		 * Make the image size equal to the panel size, by resizing it. 
169 		 */ 
 		RESIZE { 
 			@Override 
 			public void drawImage(JPanel panel, Graphics2D g2d, 
 					BufferedImage image) { 
 				g2d.drawImage(image, 0, 0, panel.getWidth(), panel.getHeight(), 
 						null); 
 			} 
 		}, 
 
 
		/** 
180 		 * Centers the image on the panel. 
181 		 */ 
 		CENTER { 
 			@Override 
 			public void drawImage(JPanel panel, Graphics2D g2d, 
 					BufferedImage image) { 
 				int left = (panel.getWidth() - image.getWidth()) / 2; 
 				int top = (panel.getHeight() - image.getHeight()) / 2; 
 				g2d.drawImage(image, left, top, null); 
 			} 
 
 
 		}, 
 		/** 
193 		 * Makes several copies of the image in the panel, putting them side by 
194 		 * side. 
195 		 */ 
 		SIDE_BY_SIDE { 
 			@Override 
 			public void drawImage(JPanel panel, Graphics2D g2d, 
 					BufferedImage image) { 
 				Paint p = new TexturePaint(image, new Rectangle2D.Float(0, 0, 
 						image.getWidth(), image.getHeight())); 
 				g2d.setPaint(p); 
 				g2d.fillRect(0, 0, panel.getWidth(), panel.getHeight()); 
 			} 
 		}; 
 
 
 		public abstract void drawImage(JPanel panel, Graphics2D g2d, 
 				BufferedImage image); 
 	} 
 
 
     @Override 
     public Dimension getPreferredSize() { 
         return images == null ? new Dimension() : images.getSize(); 
    } 
 
 
     @Override 
     protected void finalize() throws Throwable { 
         loop = false; 
        super.finalize(); 
     } 
 
 
     private class Looper extends Thread { 
 		public Looper() { 
 			setDaemon(true); 
 		} 
 
 
 		public void run() { 
 			while (loop) { 
 				repaint(); 
 				try { 
 					sleep(images.tick); 
 				} catch (Exception e) { 
					e.printStackTrace(); 
 				} 
 			} 
 		} 
 	} 
 } 

