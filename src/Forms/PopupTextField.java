/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forms;

import Capturas.CaptureMouse;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
//import javax.swing.JTextComponent;
import javax.swing.text.JTextComponent;

/**
 *
 * @author valci
 */
public class PopupTextField extends JPopupMenu {

    private JRadioButton item1, item2, item3;
    private JMenuItem item4;

    private int tamanhoDaString, fimDaSeleção, inicioDaSeleção;

  
      // build poup menu

    public PopupTextField(Principal p,CaptureMouse captureMouse) {


        java.awt.Font f=new java.awt.Font("Segoe UI", 0, 20);
        // New project menu item
        ButtonGroup bg=new ButtonGroup();
        item1 = new JRadioButton("Letras");
        bg.add(item1);
       item1.setFont(f); // NOI18N

        item1.addItemListener(new java.awt.event.ItemListener() {

            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
            if (item1.isSelected()) {
            captureMouse.setLimiteInferior(65);
            captureMouse.setLimiteSuperior(90);
            captureMouse.setLetraAtual(captureMouse.getLimiteInferior());
        }  
                p.jRadioButton1.setSelected(true);
            }
        });
        this.add(item1);
        item2 = new JRadioButton("Números");
        bg.add(item2);
        item2.setFont(f);
         item2.addItemListener(new java.awt.event.ItemListener() {

            public void itemStateChanged(java.awt.event.ItemEvent evt) {
              if (item2.isSelected()) {
         captureMouse.setLimiteInferior(48);
         captureMouse.setLimiteSuperior(57);
         captureMouse.setLetraAtual(captureMouse.getLimiteInferior());
     }
                p.jRadioButton2.setSelected(true);
            }
        });
        this.add(item2);

        item3 = new JRadioButton("Pontuação");
        item3.setFont(f);
        item3.addItemListener(new java.awt.event.ItemListener() {

            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                 if (item3.isSelected()) {
         captureMouse.setLimiteInferior(44);
         captureMouse.setLimiteSuperior(47);
         captureMouse.setLetraAtual(captureMouse.getLimiteInferior());
     }
                p.jRadioButton3.setSelected(true);
            }
        });
        bg.add(item3);
        this.add(item3);
        String caps=(Principal.isCapsAtivado())?"Desativar":"Ativar";
        item4 = new JMenuItem(caps+" Caps Lock");
       item4.setFont(f);
        item4.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                try {
        Robot r=new Robot();
        r.keyPress(KeyEvent.VK_CAPS_LOCK);
        r.keyRelease(KeyEvent.VK_CAPS_LOCK);
        
    } catch (AWTException ex) {
        Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
    }
            }
        });
        this.add(item4);
        JMenuItem item5 = new JMenuItem("Fechar este menu");
       item5.setFont(f);
        item5.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
            return ;
    }
            }
        );
        this.add(item5);
        if(p.jRadioButton1.isSelected()){
        item1.setSelected(true);
        }else if(p.jRadioButton2.isSelected()){
        item2.setSelected(true);
        }else{
        item3.setSelected(true);
        }
    
this.setBorder(null);
    }

}
